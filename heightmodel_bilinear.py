import numpy
import paths


def heightbounds(hpatch):
    return hpatch.min(), hpatch.max()


def divide_hpatch(hpatch):
    h = numpy.zeros((3, 3), dtype=float)
    h[0, 0] = hpatch[0, 0]
    h[0, 1] = (hpatch[0, 0] + hpatch[0, 1])/2
    h[0, 2] = hpatch[0, 1]
    h[1, 0] = (hpatch[0, 0] + hpatch[1, 0])/2
    h[1, 1] = hpatch.mean()
    h[1, 2] = (hpatch[0, 1] + hpatch[1, 1])/2
    h[2, 0] = hpatch[1, 0]
    h[2, 1] = (hpatch[1, 0] + hpatch[1, 1])/2
    h[2, 2] = hpatch[1, 1]

    subhpatches = (h[:2, :2], h[:2, 1:], h[1:, :2], h[1:, 1:])
    return subhpatches


SHIFT = (
    (0, 0),
    (0, 0.5),
    (0.5, 0),
    (0.5, 0.5),
    )


def subelementary_contour(hpatch, dh_max=None, dl_max=None, degree=1):
    """
    Divide given hpatch into 4 parts and perform contour algorithm on subcells.
    """
    if dl_max is not None:
        dl_max *= 4
    P = []
    for i, subhpatch in enumerate(divide_hpatch(hpatch)):
        p = elementary_contour(subhpatch, dh_max, degree)
        P.extend(map(lambda x: x.scale(0.5, 0.5).shift(*SHIFT[i]), p))
    return paths.merge_paths(P)


def intersect_line(p0, p1):
    dx = p1[0] - p0[0]
    dy = p1[1] - p0[1]
    t = p0[2]/float(p0[2] - p1[2])
    return (p0[0] + t*dx, p0[1] + t*dy)


def intersect_hpatch(hpatch):
    """
    Intersection of inner cell border with plane z=c.

    hpatch defines a bicubic polynom by specifying the values at the grid
    points where x=[-1, 0, 1, 2] and y=[-1, 0, 1, 2]. The borders of the
    inner cell with the corners at (0, 0), (0, 1), (1, 0) and (1, 1) are
    then given by a cubic polynomial which is intersected with the constant c.

    This can function can fail in different ways:
        - One edge of the inner cell intersects more than once.
        - All 4 edges of the inner cell are intersected.
    If this is the case a GeometryError is raised. Therefore it is assured
    that always exactly 2 or 0 points are returned.

    **Arguments**
        *hpatch*
            4x4 numpy array defining the bicubic polynomial. It specifies the
            values p(x,y) over a equidistant grid between the points
            (-1,-1) and (3,3).

        *c*
            Number specifying the constant with which the polynomial is
            intersected.

    **Returns**
        *intersections*
            Either 2 points where the bicubic polynomial and the constant plane
            intersect at the inner cell border or **None** if no intersection
            exists.
    """
    intersections = set()
    is_below = (hpatch < 0)
    is_above = (hpatch > 0)
    is_equal = (hpatch == 0)
    N_below = is_below.sum()
    N_above = is_above.sum()
    N_equal = is_equal.sum()
    if N_below == 4 or N_above == 4:
        pass
    elif N_below >= 1 and N_above >= 1:
        p0 = [0, 0, hpatch[0, 0]]
        p1 = [0, 1, hpatch[0, 1]]
        p2 = [1, 1, hpatch[1, 1]]
        p3 = [1, 0, hpatch[1, 0]]
        if p0[2]*p1[2] <= 0:
            intersections.add(intersect_line(p0, p1))
        if p1[2]*p2[2] <= 0:
            intersections.add(intersect_line(p1, p2))
        if p2[2]*p3[2] <= 0:
            intersections.add(intersect_line(p2, p3))
        if p3[2]*p0[2] <= 0:
            intersections.add(intersect_line(p3, p0))
    elif N_equal == 1 and (N_above == 3 or N_below == 3):
        pass
    elif N_equal == 2:
        # If they are on diagonal corners its not intersecting
        if (is_equal == [[True, False], [False, True]]).all()\
                or (is_equal == [[False, True], [True, False]]).all():
            pass
        else:
            if is_equal[0, 0]:
                intersections.add((0., 0.))
            if is_equal[0, 1]:
                intersections.add((0., 1.))
            if is_equal[1, 0]:
                intersections.add((1., 0.))
            if is_equal[1, 1]:
                intersections.add((1., 1.))
    elif N_equal == 3:
        raise NotImplementedError()
    return intersections


def intersect_lines(p0, u0, p1, u1):
    c = 1./(u1[0]*u0[1] - u1[1]*u0[0])
    t = c*(p1[1] - p0[1])*u0[0] - c*(p1[0] - p0[0])*u0[1]
    return p1[0] + t*u1[0], p1[1] + t*u1[1]


def gradient(hpatch, x, y):
    a00 = hpatch[0, 0]
    a01 = hpatch[0, 1] - a00
    a10 = hpatch[1, 0] - a00
    a11 = hpatch[1, 1] - a10 - a01 - a00
    return (a10 + a11*y, a01 + a11*x)


def ode_contour(hpatch, p0, dl_max):
    grad = gradient(hpatch, *p0)
    direction = [grad[1], -grad[0]]
    if p0[0] == 0.:
        sign = numpy.sign(direction[0])
    elif p0[0] == 1.:
        sign = -numpy.sign(direction[0])
    elif p0[1] == 0.:
        sign = numpy.sign(direction[1])
    elif p0[1] == 1.:
        sign = -numpy.sign(direction[1])

    def f(t, p, pnew):
        grad = gradient(hpatch, *p)
        grad /= numpy.linalg.norm(grad)
        pnew[0] = sign*grad[1]
        pnew[1] = -sign*grad[0]

    def outside(t, p):
        if (p < 0).any() or (p > 1).any():
            return 1
        else:
            return -1
    import ode_dopri
    T = numpy.arange(0, 10, dl_max)
    tout, pout = ode_dopri.ode(
                    f, T, numpy.array(p0),
                    display_beforeevent=True,
                    event_locator=outside,
                    event_callback=lambda t, p: "stop"
                    )
    return pout


def elementary_contour(hpatch, dh_max=None, dl_max=None, degree=1):
    """
    Generate contour lines of bicubic polynomial.

    **Arguments**
        *height*
            Float specifying for which height the contourline should
            be generated.

        *hpatch*
            4x4 numpy array defining the bicubic polynomial.

        *dh_max*
            Maximal height difference of the bicubic polynomial over the inner
            cell. If this limit is exceeded the cell is subdivided and the
            algorithm is applied recursively.
    """
    assert hpatch.shape[0] == 2
    assert hpatch.shape[1] == 2
    if degree > 2:
        raise NotImplementedError()
    h_min, h_max = heightbounds(hpatch)
    if 0 < h_min or h_max < 0:
        return []

    if dh_max and h_max-h_min > dh_max:
        return subelementary_contour(hpatch, dh_max=dh_max, dl_max=dl_max, degree=degree)

    intersections = intersect_hpatch(hpatch)
    if dl_max is None:
        N = len(intersections)
        if N == 0:
            return []
        if N > 2:
            return subelementary_contour(hpatch, dh_max=dh_max, dl_max=dl_max, degree=degree)
        assert N == 2
        p0 = intersections.pop()
        p1 = intersections.pop()
        if degree == 1:
            x = (p0[0], p1[0])
            y = (p0[1], p1[1])
            return [paths.Path(x, y, degree=1, closed=False)]
        elif degree == 2:
            u0 = gradient(hpatch, *p0)
            v0 = (u0[1], -u0[0])
            u1 = gradient(hpatch, *p1)
            v1 = (u1[1], -u1[0])
            px, py = paths.intersect_lines(p0, v0, p1, v1)
            if px < 0 or 1 < px or py < 0 or 1 < py:
                norm = numpy.linalg.norm
                v = [p1[0] - p0[0], p1[1] - p0[1]]
                angle1 = numpy.dot(v0, v1)/(norm(v0)*norm(v1))
                angle2 = numpy.dot(v0, v)/(norm(v0)*norm(v))
                if numpy.arccos(angle1) < 1e-2 and numpy.arccos(angle2) < 1e-2:
                    px = (p0[0] + p1[0])/2.
                    py = (p0[1] + p1[1])/2.
                else:
                    print("Controlpoint not in boundary.")
                    # print("Tangents angle:", numpy.arccos(angle1))
                    return subelementary_contour(hpatch, dh_max=dh_max, dl_max=dl_max, degree=degree)
            x = (p0[0], px, p1[0])
            y = (p0[1], py, p1[1])
            return [paths.Path(x, y, degree=2, closed=False)]
        else:
            raise NotImplementedError()


    P = []
    while intersections:
        intersection = intersections.pop()
        points = ode_contour(hpatch, intersection, dl_max)
        points = numpy.array(points)
        x = points[:, 0]
        y = points[:, 1]
        for p in intersections:
            if (p[0] - x[-1])**2 + (p[1] - y[-1])**2 < 1e-5:
                intersections.remove(p)
                x[-1] = p[0]
                y[-1] = p[1]
                break
        else:
            print("ODE endpoint: ", x[-1], y[-1])
            print("intersection points: ", intersections)
            return subelementary_contour(hpatch, dh_max=dh_max, dl_max=dl_max, degree=degree)
            raise Exception("Endpoint of ode path not an intersection point.")

        if degree == 1:
            P.append(paths.Path(x, y, degree=1, closed=False))
        elif degree == 2:
            tangents = []
            for i in range(len(x)):
                u0 = gradient(hpatch, x[i], y[i])
                tangents.append((u0[1], -u0[0]))
            bezierpoints_x = []
            bezierpoints_y = []

            for i in range(len(x)-1):
                px, py = paths.intersect_lines((x[i], y[i]), tangents[i], (x[i+1], y[i+1]), tangents[i+1])
                if px < 0 or 1 < px or py < 0 or 1 < py:
                    norm = numpy.linalg.norm
                    v = [x[i+1] - x[i], y[i+1] - y[i]]
                    v0 = tangents[i]
                    v1 = tangents[i+1]
                    angle1 = numpy.dot(v0, v1)/(norm(v0)*norm(v1))
                    angle2 = numpy.dot(v0, v)/(norm(v0)*norm(v))
                    if numpy.arccos(angle1) < 1e-2 and numpy.arccos(angle2) < 1e-2:
                        px = (x[i] + x[i+1])/2.
                        py = (y[i] + y[i+1])/2.
                    else:
                        # print("Controlpoint not in boundary.")
                        # print("Tangents angle:", numpy.arccos(angle1))
                        print("Warning: Controlpoint not in boundary - Subdividing!")
                        return subelementary_contour(hpatch, dh_max=dh_max, dl_max=dl_max, degree=degree)
                bezierpoints_x.append(x[i])
                bezierpoints_x.append(px)
                bezierpoints_y.append(y[i])
                bezierpoints_y.append(py)
            bezierpoints_x.append(x[-1])
            bezierpoints_y.append(y[-1])
            P.append(paths.Path(bezierpoints_x, bezierpoints_y, degree=2, closed=False))
        else:
            raise NotImplementedError()
    return P


def contour(height, hmap, dh_max=None, dl_max=None, degree=1):
    Nx, Ny = hmap.shape
    contours = []
    for i in range(Nx-1):
        for j in range(Ny-1):
            hpatch = hmap[i:i+2, j:j+2] - height
            for p in elementary_contour(hpatch, dh_max=dh_max, dl_max=dl_max, degree=degree):
                contours.append(p.shift(i, j).scale(1./(Nx-1), 1./(Ny-1)))
    return paths.merge_paths(contours)


def interpolate(hmap, xlim, ylim):
    Nx, Ny = hmap.shape
    def f(x, y):
        x_ = (x - xlim[0])/(xlim[1] - xlim[0])*(Nx-1)
        y_ = (y - ylim[0])/(ylim[1] - ylim[0])*(Ny-1)
        i = int(x_)
        j = int(y_)
        x_ -= i
        y_ -= j
        a00 = hmap[i, j]
        a01 = hmap[i, j+1] - a00
        a10 = hmap[i+1, j] - a00
        a11 = hmap[i+1, j+1] - a00 - a01 - a10
        return a00 + a10*x_ + a01*y_ + a11*x_*y_
    return f
