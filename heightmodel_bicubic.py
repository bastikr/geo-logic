import numpy
import scipy.interpolate
import polynomials
import bezier_surface
import paths
import tree


def heightbounds(hpatch):
    """
    Upper and lower bounds for the bicubic polynomial defined by hpatch.

    This function is implemented by using the fact that a bezier surface always
    is completely inside the bounding box created by its bezier control points.

    **Arguments**
        *hpatch*
            4x4 numpy array defining the bicubic polynomial. It is assumed that
            these values specify the height over a 4x4 grid between the points
            (-1, 0, 1, 2) x (-1, 0, 1, 2).

    **Returns**
        *hmin*
            Minimum bound of bicubic polynomial over the area between the
            points (0,0) and (1,1).

        *hmax*
            Maximum bound of bicubic polynomial over the area between the
            points (0,0) and (1,1).
    """
    c_bezier = bezier_surface.lagrange2bezier(hpatch)
    return c_bezier.min(), c_bezier.max()


def divide_hpatch(hpatch):
    """
    Split the inner cell of a hpatch into four new hpatches.

    **Arguments**
        *hpatch*
            4x4 numpy array defining the bicubic hermite spline. It is assumed
            that these values specify the height over a 4x4 grid between the
            points (-1,-1) and (2,2).

    **Returns**
        *subhpatches*
            List of hpatches where the inner cells are between
                * (0,0), (0.5,0.5)
                * (0,0.5), (0.5,1)
                * (0.5,0), (1,0.5)
                * (0.5,0.5), (1,1)
    """
    p = numpy.linspace(-0.5, 1.5, 5)
    h = numpy.zeros((5, 5), dtype=float)
    for i, x in enumerate(p):
        for j, y in enumerate(p):
            h[i, j] = bezier_surface.evaluate_lagrange_surface(hpatch, x, y)
    subhpatches = (h[:4, :4], h[:4, 1:], h[1:, :4], h[1:, 1:])
    return subhpatches


SHIFT = (
    (0, 0),
    (0, 0.5),
    (0.5, 0),
    (0.5, 0.5),
    )


def subelementary_contour(hpatch, degree, dl_max, dh_max):
    """
    Divide given hpatch into 4 parts and perform contour algorithm on subcells.
    """
    kwargs = {
        "degree": degree,
        "dh_max": dh_max,
        "dl_max": dl_max,
    }
    if dl_max is not None:
        dl_max *= 4
    P = []
    for i, subhpatch in enumerate(divide_hpatch(hpatch)):
        p = elementary_contour(subhpatch, **kwargs)
        P.extend(map(lambda x: x.scale(0.5, 0.5).shift(*SHIFT[i]), p))
    return paths.merge_paths(P)


def intersect_constant(p, c=0.):
    """
    Intersections of a cubic polynomial and a constant in the interval [0,1].

    The polynomial is defined by its values p(x) for x in {-1, 0, 1, 2}.

    **Arguments**
        *p*
            Values of the polynomial at the points x={-1, 0, 1, 2}.

        *c*
            Value of the constant line that intersects the polynomial.

    **Returns**
        Tuple of intersection points in the interval [0,1]. Can have 0-3
        elements.
    """
    eps = 1e-10
    # eps = 0.2
    coeffs = polynomials.T_lagrange2standard.dot(p)
    coeffs[0] -= c
    roots = polynomials.roots(coeffs)
    roots_inside = []
    for root in roots:
        if -eps < root < +eps:
            roots_inside.append(0.)
        elif 1-eps < root < 1+eps:
            roots_inside.append(1.)
        elif 0 <= root <= 1:
            roots_inside.append(root)
    return tuple(roots_inside)


def intersect_hpatch(hpatch, c=0.):
    """
    Intersection of inner cell border with plane z=c.

    hpatch defines a bicubic polynom by specifying the values at the grid
    points where x=[-1, 0, 1, 2] and y=[-1, 0, 1, 2]. The borders of the
    inner cell with the corners at (0, 0), (0, 1), (1, 0) and (1, 1) are
    then given by a cubic polynomial which is intersected with the constant c.

    This can function can fail in different ways:
        - One edge of the inner cell intersects more than once.
        - All 4 edges of the inner cell are intersected.
    If this is the case a GeometryError is raised. Therefore it is assured
    that always exactly 2 or 0 points are returned.

    **Arguments**
        *hpatch*
            4x4 numpy array defining the bicubic polynomial. It specifies the
            values p(x,y) over a equidistant grid between the points
            (-1,-1) and (3,3).

        *c*
            Number specifying the constant with which the polynomial is
            intersected.

    **Returns**
        *intersections*
            Either 2 points where the bicubic polynomial and the constant plane
            intersect at the inner cell border or **None** if no intersection
            exists.
    """
    gradient = interp_gradient(hpatch)

    def is_properpoint(x0, y0):
        if x0 not in [0., 1.] or y0 not in [0., 1.]:
            return (x0, y0)
        grad = gradient(x0, y0)
        sign = 1 if x0 == y0 else -1
        if sign*grad[0]*grad[1] > 0:
            return None
        elif grad[0]*grad[1] == 0.:
            raise Exception()
        else:
            return (x0, y0)

    intersections = set()
    for y0 in intersect_constant(hpatch[1, :], c):
        if is_properpoint(0., y0):
            intersections.add((0., y0))
    for y0 in intersect_constant(hpatch[2, :], c):
        if is_properpoint(1., y0):
            intersections.add((1., y0))
    for x0 in intersect_constant(hpatch[:, 1], c):
        if is_properpoint(x0, 0.):
            intersections.add((x0, 0.))
    for x0 in intersect_constant(hpatch[:, 2], c):
        if is_properpoint(x0, 1.):
            intersections.add((x0, 1.))

    assert len(intersections) % 2 == 0

    return intersections


def intersect_lines(p0, u0, p1, u1):
    c = 1./(u1[0]*u0[1] - u1[1]*u0[0])
    t = c*(p1[1] - p0[1])*u0[0] - c*(p1[0] - p0[0])*u0[1]
    return p1[0] + t*u1[0], p1[1] + t*u1[1]


def interp_gradient(hpatch):
    p = [-1, 0, 1, 2]
    interp2d = scipy.interpolate.interp2d(p, p, hpatch.T, kind="cubic")

    def gradient(u, v):
        return numpy.array([interp2d(u, v, 1, 0)[0], interp2d(u, v, 0, 1)[0]])
    return gradient


def ode_contour(hpatch, p0, dl_max, **kwargs):
    gradient = interp_gradient(hpatch)
    grad = gradient(*p0)
    direction = [grad[1], -grad[0]]
    if p0[0] == 0.:
        sign = numpy.sign(direction[0])
    elif p0[0] == 1.:
        sign = -numpy.sign(direction[0])
    elif p0[1] == 0.:
        sign = numpy.sign(direction[1])
    elif p0[1] == 1.:
        sign = -numpy.sign(direction[1])

    def f(t, p, dp):
        grad = gradient(*p)
        grad /= numpy.linalg.norm(grad)
        dp[0] = sign*grad[1]
        dp[1] = -sign*grad[0]

    def outside(t, p):
        if (p < 0).any() or (p > 1).any():
            return 1
        else:
            return -1
    import ode_dopri
    if dl_max is None:
        T = numpy.array([0, 10])
    else:
        T = numpy.arange(0, 10, dl_max)
    tout, pout = ode_dopri.ode(
                    f, T, numpy.array(p0),
                    display_beforeevent=True,
                    event_locator=outside,
                    event_callback=lambda t, p: "stop",
                    **kwargs
                    )
    return pout


class UnmatchedIntersection(Exception):
    pass


def match_intersections(hpatch, intersections, dl_max,
                        ode_tol=1e-5,
                        point_tol=1e-4):
    P = []
    norm = numpy.linalg.norm
    I = intersections.copy()
    while I:
        intersection = I.pop()
        points = ode_contour(hpatch, intersection, dl_max, reltol=ode_tol)
        for p in I:
            dp = p - points[-1]
            if norm(dp) < point_tol:
                I.remove(p)
                for i in range(1, len(points)):
                    l0 = norm(points[0] - points[i])
                    l1 = norm(points[-1] - points[i])
                    c = 1 - l1/(l0 + l1)
                    points[i] += c*dp
                break
        else:
            # print("="*80)
            # print("Can't connect intersection points:")
            # print("    Intersections: " + str(intersections))
            # print("    ODE points" + str(points))
            # print("="*80)
            raise UnmatchedIntersection()
        P.append(points)
    return P


def elementary_contour(hpatch, degree=2, dh_max=None, dl_max=None):
    """
    Generate contour lines of bicubic polynomial.

    **Arguments**
        *hpatch*
            4x4 numpy array defining the bicubic polynomial.

        *degree*
            Number specifying the degree of the returned contour paths.
            * degree=1: Straight lines
            * degree=2: Quadratic Bezier polynoms

        *dh_max*
            Maximal height difference of the bicubic polynomial over the inner
            cell. If this limit is exceeded the cell is subdivided and the
            algorithm is applied recursively.

        *dl_max*
            Maximal distance between points of the resulting contour path. If
            it is None only points on the boundary are included.
    """
    kwargs = {
        "degree": degree,
        "dh_max": dh_max,
        "dl_max": dl_max,
    }

    assert hpatch.shape[0] == 4
    assert hpatch.shape[1] == 4

    h_min, h_max = heightbounds(hpatch)
    if not h_min <= 0 <= h_max:
        return []

    if dh_max is not None and h_max-h_min > dh_max:
        return subelementary_contour(hpatch, **kwargs)

    intersections = intersect_hpatch(hpatch)
    try:
        connections = match_intersections(hpatch, intersections, dl_max,
                                          ode_tol=1e-7, point_tol=1e-5)
    except UnmatchedIntersection:
        return subelementary_contour(hpatch, **kwargs)

    P = []
    if degree == 1:
        for points in connections:
            P.append(paths.Path(points, degree=1, closed=False))
    elif degree == 2:
        gradient = interp_gradient(hpatch)
        for points in connections:
            tangents = []
            for i in range(len(points)):
                u0 = gradient(*points[i])
                t0 = numpy.array((u0[1], -u0[0]))
                tangents.append(t0/numpy.linalg.norm(t0))
            bezierpoints = []

            for i in range(len(points)-1):
                p0 = points[i]
                p1 = points[i+1]
                px, py = intersect_lines(p0, tangents[i], p1, tangents[i+1])
                p = numpy.array([px, py])
                norm = numpy.linalg.norm
                dp = p1 - p0
                v0 = tangents[i]
                v1 = tangents[i+1]
                angle1 = numpy.dot(v0, dp)/(norm(v0)*norm(dp))
                angle2 = numpy.dot(v1, dp)/(norm(v1)*norm(dp))
                if 1 - abs(angle1) < 1e-6 and 1 - abs(angle2) < 1e-6:
                    p = (p0 + p1)/2.
                elif not 0.1 < dp.dot(p - p0)/norm(dp)**2 < 0.9:
                    # print("Control-point not satisfying.")
                    return subelementary_contour(hpatch, **kwargs)
                elif px < 0 or 1 < px or py < 0 or 1 < py:
                    # print("Control-point out of boundary.")
                    return subelementary_contour(hpatch, **kwargs)
                bezierpoints.append(p0)
                bezierpoints.append(p)
            bezierpoints.append(points[-1])
            P.append(paths.Path(bezierpoints, degree=2, closed=False))
    else:
        raise NotImplementedError()
    return P


def contour(height, hmap, degree=2, dh_max=None, dl_max=None):
    kwargs = {
        "degree": degree,
        "dh_max": dh_max,
        "dl_max": dl_max,
    }
    Nx, Ny = hmap.shape
    contours = []
    for i in range(1, Nx-2):
        for j in range(1, Ny-2):
            hspline = hmap[i-1:i+3, j-1:j+3]
            if numpy.isnan(hspline).any():
                continue
            hpatch = bezier_surface.hpatch2lagrange(hspline) - height
            for p in elementary_contour(hpatch, **kwargs):
                contours.append(p.shift(i-1, j-1))
    return paths.merge_paths(contours)


def contour_tree(height, treenode, degree=2, dh_max=None, dl_max=None):
    kwargs = {
        "degree": degree,
        "dh_max": dh_max,
        "dl_max": dl_max,
    }
    contours = []
    if not treenode.hmin <= height <= treenode.hmax:
        pass
    elif treenode.children is None:
        patch = treenode.data
        if not numpy.isnan(patch.data).any():
            hpatch = patch.data - height
            for p in elementary_contour(hpatch, **kwargs):
                contours.append(p.shift(patch.i, patch.j))
    else:
        for node in treenode.children:
            contours.extend(contour_tree(height, node, **kwargs))
        contours = paths.merge_paths(contours)
    return contours


def create_tree(hmap, i=0, j=0):
    Nx = hmap.shape[0] - 3
    Ny = hmap.shape[1] - 3
    assert Nx > 0, (Nx, Ny)
    assert Ny > 0, (Nx, Ny)
    if Nx == 1 and Ny == 1:
        if numpy.isnan(hmap).any():
            hpatch = None
            hmin = hmax = numpy.NaN
        else:
            hpatch = bezier_surface.hpatch2lagrange(hmap)
            hmin, hmax = heightbounds(hpatch)
        return tree.Node(hmin, hmax, None, tree.Patch(i, j, hpatch))
    if Nx >= Ny:
        nx = Nx//2
        node0 = create_tree(hmap[:nx+3, :], i=i, j=j)
        node1 = create_tree(hmap[nx:, :], i=i+nx, j=j)
    else:
        ny = Ny//2
        node0 = create_tree(hmap[:, :ny+3], i=i, j=j)
        node1 = create_tree(hmap[:, ny:], i=i, j=j+ny)
    hmin = numpy.nanmin([node0.hmin, node1.hmin])
    hmax = numpy.nanmax([node0.hmax, node1.hmax])
    return tree.Node(hmin, hmax, (node0, node1), None)


def interpolate(hmap, xlim, ylim):
    Nx, Ny = hmap.shape
    Nx -= 2
    Ny -= 2
    # px = numpy.arange(-1, Nx+1)
    # py = numpy.arange(-1, Ny+1)
    # hpatch = bezier_surface.hpatch2lagrange(hmap)
    # return scipy.interpolate.interp2d(px, py, hmap.T, kind="cubic")
    def f(x, y):
        if not xlim[0] <= x <= xlim[1] or not ylim[0] <= y <= ylim[1]:
            raise ValueError("point out of interpolation bounds.")
        i, x_ = divmod(x - xlim[0], (xlim[1] - xlim[0])/(Nx-1))
        j, y_ = divmod(y - ylim[0], (ylim[1] - ylim[0])/(Ny-1))
        if i > 0 and x_ == 0:
            i -= 1
            x_ = 1.
        if j > 0 and y_ == 0:
            j -= 1
            y_ = 1.
        hpatch = hmap[i:i+4, j:j+4]
        assert hpatch.shape[0] == 4, (x, y, xlim, ylim, (i, j), (x_, y_))
        assert hpatch.shape[1] == 4, (x, y, xlim, ylim, (i, j), (x_, y_))
        return bezier_surface.evaluate_hpatch_surface(hpatch, x_, y_)
    return f
