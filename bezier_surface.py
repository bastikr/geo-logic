import polynomials
import numpy
import scipy.interpolate


def height2bezier(hpatch):
    """
    Transforms the hpatch heights to bezier control points.

    hpatch defines a bicubic polynomial by specifying the z-values at
    the grid points [-1, 0, 1, 2] x [-1, 0, 1, 2]. This polynomial can
    equivalently be defined by bezier control points which are in this
    case assumed to be on the grid [0, 1/3, 2/3, 1] x [0, 1/3, 2/3, 1].
    """
    T = polynomials.T_lagrange2bezier
    return T.dot(hpatch).dot(T.T)


def bezier2height(hbezier):
    """
    Transforms bezier control points to hpatch heigts.

    hbezier defines a bicubic polynomial by specifying bezier control
    points which are in this case assumed to be on the
    grid [0, 1/3, 2/3, 1] x [0, 1/3, 2/3, 1].
    This polynomial can equivalently be defined by the z-values at
    the grid points [-1, 0, 1, 2] x [-1, 0, 1, 2].
    """
    T = polynomials.T_bezier2lagrange
    return T.dot(hbezier).dot(T.T)


def hpatch2bezier(hpatch):
    """
    Transforms the hpatch heights to bezier control points.

    hpatch defines a bicubic polynomial by specifying the z-values at
    the grid points [-1, 0, 1, 2] x [-1, 0, 1, 2]. This polynomial can
    equivalently be defined by bezier control points which are in this
    case assumed to be on the grid [0, 1/3, 2/3, 1] x [0, 1/3, 2/3, 1].
    """
    T = polynomials.T_hpatch2bezier
    return T.dot(hpatch).dot(T.T)


def bezier2hpatch(hbezier):
    """
    Transforms bezier control points to hpatch heigts.

    hbezier defines a bicubic polynomial by specifying bezier control
    points which are in this case assumed to be on the
    grid [0, 1/3, 2/3, 1] x [0, 1/3, 2/3, 1].
    This polynomial can equivalently be defined by the z-values at
    the grid points [-1, 0, 1, 2] x [-1, 0, 1, 2].
    """
    T = polynomials.T_bezier2hpatch
    return T.dot(hbezier).dot(T.T)


def hpatch2standard(hpatch):
    T = polynomials.T_hpatch2standard
    return T.dot(hpatch).dot(T.T)


def lagrange2bezier(heights):
    T = polynomials.T_lagrange2bezier
    return T.dot(heights).dot(T.T)


def lagrange2hpatch(heights):
    T = polynomials.T_lagrange2hpatch
    return T.dot(heights).dot(T.T)


def hpatch2lagrange(hpatch):
    T = polynomials.T_hpatch2lagrange
    return T.dot(hpatch).dot(T.T)


def evaluate_lagrange_surface(hpatch, u, v):
    T = polynomials.T_lagrange2standard
    coeff_matrix = T.dot(hpatch).dot(T.T)
    return polynomials.horner2d(coeff_matrix, u, v)


def evaluate_hpatch_surface(hpatch, u, v):
    T = polynomials.T_hpatch2standard
    coeff_matrix = T.dot(hpatch).dot(T.T)
    return polynomials.horner2d(coeff_matrix, u, v)


def evaluate_bezier_surface(B, u, v):
    T = polynomials.T_bezier2standard
    coeff_matrix = T.dot(B).dot(T.T)
    return polynomials.horner2d(coeff_matrix, u, v)


# def gradient(hpatch, u, v):
#     U = polynomials.powervector(u)
#     V = polynomials.powervector(v)
#     dU = polynomials.dpowervector(u)
#     dV = polynomials.dpowervector(v)
#     T = polynomials.T_hpatch2standard
#     dfdx = dU.dot(T).dot(hpatch).dot(T.T).dot(V)
#     dfdy = U.dot(T).dot(hpatch).dot(T.T).dot(dV)
#     return [dfdx, dfdy]


# def gradient(hpatch, u, v):
#     T = polynomials.T_lagrange2standard
#     coeff_matrix = T.dot(hpatch).dot(T.T)
#     dfdx = polynomials.dhorner2d(coeff_matrix, u, v, 1, 0)
#     dfdy = polynomials.dhorner2d(coeff_matrix, u, v, 0, 1)
#     return numpy.array([dfdx, dfdy])


def gradient(hpatch, u, v):
    p = [-1, 0, 1, 2]
    f = scipy.interpolate.interp2d(p, p, hpatch.T, kind="cubic")
    return numpy.array([f([u], [v], 1, 0)[0], f([u], [v], 0, 1)[0]])
