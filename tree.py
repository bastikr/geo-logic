import collections

Node = collections.namedtuple("Node", ["hmin", "hmax", "children", "data"])
Patch = collections.namedtuple("Patch", ["i", "j", "data"])
