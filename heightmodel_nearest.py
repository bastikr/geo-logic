import numpy
import paths


def heightbounds(hpatch):
    return hpatch.min(), hpatch.max()


def simplify_path(path):
    direction = path.points[1] - path.points[0]
    p0 = path.points[1]
    points = [p0]
    for p in path.points[2:]:
        new_direction = p - p0
        if not numpy.linalg.norm(new_direction - direction) < 1e-5:
            points.append(p0)
            direction = new_direction
        p0 = p
    points.append(p0)
    return paths.Path(points, degree=1, closed=path.closed)


def elementary_contour(hpatch):
    assert hpatch.shape == (2, 2)

    h_min, h_max = heightbounds(hpatch)
    if not h_min < 0 < h_max:
        return []

    P = []
    # Find points where the isoline intersects with the boundary.
    if hpatch[0, 0]*hpatch[0, 1] < 0:
        P.append([0., 0.5])
    if hpatch[0, 1]*hpatch[1, 1] < 0:
        P.append([0.5, 1])
    if hpatch[1, 1]*hpatch[1, 0] < 0:
        P.append([1, 0.5])
    if hpatch[1, 0]*hpatch[0, 0] < 0:
        P.append([0.5, 0])

    # Take care of situations where the isoline intersects a corner point.
    P.extend(numpy.transpose(numpy.where(hpatch == 0.)))

    # Connect intersection points on the boundary with the middle point.
    pm = numpy.array([0.5, 0.5])
    P = (paths.Path([numpy.array(p), pm], degree=1, closed=False) for p in P)

    return paths.merge_paths(P)


def contour(height, hmap, degree):
    Nx, Ny = hmap.shape
    contours = []
    for i in range(Nx-1):
        for j in range(Ny-1):
            hpatch = hmap[i:i+2, j:j+2] - height
            for p in elementary_contour(hpatch):
                contours.append(p.shift(i, j))
    P = paths.merge_paths(contours)
    return list(simplify_path(p) for p in P)


def interpolate(hmap, xlim, ylim):
    Nx, Ny = hmap.shape

    def f(x, y):
        x_ = (x - xlim[0])/float(xlim[1] - xlim[0])*(Nx-1)
        y_ = (y - ylim[0])/float(ylim[1] - ylim[0])*(Ny-1)
        i = int(x_ + 0.5)
        j = int(y_ + 0.5)
        return hmap[i, j]
    return f
