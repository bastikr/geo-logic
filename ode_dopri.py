
# using Roots

import numpy
import scipy.optimize

order = 5
a2 = [1./5]
a3 = [3./40, 9./40]
a4 = [44./45, -56./15, 32./9]
a5 = [19372./6561, -25360./2187, 64448./6561, -212./729]
a6 = [9017./3168, -355./33, 46732./5247, 49./176, -5103./18656]
a7 = [35./384, 0., 500./1113, 125./192, -2187./6784, 11./84]
a = [[], a2, a3, a4, a5, a6, a7]
bs = [5179./57600, 0., 7571./16695, 393./640, -92097./339200, 187./2100, 1./40]
c = [0., 1./5, 3./10, 4./5, 8./9, 1., 1.]


def b7(theta):
    return (theta**2*(theta-1) + theta**2*(theta-1)**2
            * 10*(7414447 - 829305*theta)/29380423.)


def b1(theta):
    return (theta**2*(3-2*theta)*a7[0] + theta*(theta-1)**2
            - theta**2*(theta-1)**2*5*(2558722523-31403016*theta)/11282082432.)


def b3(theta):
    return (theta**2*(3-2*theta)*a7[2] + theta**2*(theta-1)**2
            * 100*(882725551 - 15701508*theta)/32700410799.)


def b4(theta):
    return (theta**2*(3-2*theta)*a7[3] - theta**2*(theta-1)**2
            * 25*(443332067 - 31403016*theta)/1880347072.)


def b5(theta):
    return (theta**2*(3-2*theta)*a7[4] + theta**2*(theta-1)**2
            * 32805*(23143187 - 3489224*theta)/199316789632.)


def b6(theta):
    return (theta**2*(3-2*theta)*a7[5] - theta**2*(theta-1)**2
            * 55*(29972135 - 7076736*theta)/822651844.)


def substep(x, x0, h, coeffs, k):
    for m in range(len(x0)):
        dx = 0.
        for i in range(len(coeffs)):
            dx += coeffs[i]*k[i][m]
        x[m] = x0[m] + h*dx


def step(F, t, h, x0, xp, xs, k):
    #for i=2:length(c)
    for i in range(1, len(c)):
        substep(xp, x0, h, a[i], k)
        F(t + h*c[i], xp, k[i])
    substep(xs, x0, h, bs, k)


def allocate_memory(x):
    xp = numpy.zeros_like(x)
    xs = numpy.zeros_like(x)
    k = []
    for i in range(7):
        k.append(numpy.zeros_like(x))
    return (xp, xs, k)


def interpolate(t0, x0, h, k, t, x):
    theta = (t-t0)/h
    b1_ = b1(theta)
    b3_ = b3(theta)
    b4_ = b4(theta)
    b5_ = b5(theta)
    b6_ = b6(theta)
    b7_ = b7(theta)
    # for i=1:length(x0)
    for i in range(len(x0)):
        dx = b1_*k[0][i] + b3_*k[2][i] + b4_*k[3][i]
        dx += b5_*k[4][i] + b6_*k[5][i] + b7_*k[6][i]
        x[i] = x0[i] + h*dx


def abs2(x):
    return numpy.abs(x)**2


def error_estimate(xp, xs, abstol, reltol):
    err = 0.
    # for i=1:length(xp)
    for i in range(len(xp)):
        sc_i = abstol + reltol*max(abs(xp[i]), abs(xs[i]))
        err += abs2(xp[i] - xs[i])/sc_i**2
    return numpy.sqrt(err/len(xp))


def initial_stepsize(F, t, x, k, abstol, reltol, tmp1, tmp2):
    d0 = 0.
    d1 = 0.
    for i in range(len(x)):
        sc_i2 = (abstol + abs(x[i])*reltol)**2
        d0 += abs2(x[i])/sc_i2
        d1 += abs2(k[0][i])/sc_i2
    d0 = numpy.sqrt(d0/len(x))
    d1 = numpy.sqrt(d1/len(x))
    # h0 = ((d0<1e-5 || d1<1e-5) ? 1e-6 : 0.01*d0/d1)
    if d0 < 1e-5 or d1 < 1e-5:
        h0 = 1e-6
    else:
        h0 = 0.01*d0/d1
    substep(tmp1, x, h0, [1.], k)
    F(t+h0, tmp1, tmp2)
    # for i=1:length(tmp2)
    for i in range(len(tmp2)):
        tmp2[i] -= k[0][i]
    d2 = numpy.linalg.norm(tmp2, 2)
    if max(d1, d2) < 1e-15:
        h1 = max(1e-6, h0*1e-3)
    else:
        h1 = (0.01/max(d1, d2))**(1./order)
    return min(100*h0, h1)


def stepsize_strategy(err, laststepaccepted, h, hmin, hmax):
    accept_step = (err < 1)
    # facmin = (laststepaccepted ? 5. : 1.)
    facmin = (5. if laststepaccepted else 1.)
    if err == 0.:
        hnew = h*facmin
    else:
        hnew = h*min(facmin, max(0.2, 0.9*(1./err)**(1./order)))
    hnew = min(hmax, hnew)
    if hnew < hmin:
        raise Exception("Stepsize below hmin.")
    return hnew, accept_step


def display_steps(fout, tspan, t, x, h, k, xs):
    for tout in tspan:
        if t < tout <= t+h:
            interpolate(t, x, h, k, tout, xs)
            fout(tout, xs)


def ode(F, tspan, x0,
        reltol=1.0e-7,
        abstol=1.0e-8,
        h0=None,
        hmin=None,
        hmax=None,
        display_initialvalue=True,
        display_finalvalue=True,
        display_intermediatesteps=False,
        display_beforeevent=False,
        display_afterevent=False,
        fout=None,
        # event_locator::Function=(t,x)->1.,
        # event_callback::Function=(t,x)->"continue",
        event_locator=lambda t, x: 1.,
        event_callback=lambda t, x: "continue"):
    if hmin is None:
        hmin = (tspan[-1] - tspan[0])/1e9
    if hmax is None:
        hmax = tspan[-1] - tspan[0]
    if fout is None:
        tout = []
        xout = []

        def fout_(t, x):
            tout.append(t)
            xout.append(x.copy())
    else:
        fout_ = fout
    tspan = numpy.array(tspan)
    t, tfinal = tspan[0], tspan[-1]
    if display_initialvalue:
        fout_(t, x0)
    x = x0.copy()
    xp, xs, k = allocate_memory(x0)
    F(t, x, k[0])
    if h0 is None:
        h = initial_stepsize(F, t, x, k, abstol, reltol, k[1], k[2])
    else:
        h = h0
    h = max(hmin, h)
    h = min(hmax, h)
    accept_step = True
    while t < tfinal:
        step(F, t, h, x, xp, xs, k)
        err = error_estimate(xp, xs, abstol, reltol)
        hnew, accept_step = stepsize_strategy(err, accept_step, h, hmin, hmax)
        if accept_step:
            if numpy.sign(event_locator(t, x)) != numpy.sign(event_locator(t + h, xp)):
                # t_event = fzero(t_->(interpolate(t, x, h, k, t_, xs); event_locator(t_, xs)), t, t+h)
                def eventfunction(t_):
                    interpolate(t, x, h, k, t_, xs)
                    return event_locator(t_, xs)
                t_event = scipy.optimize.bisect(eventfunction, t, t + h)
                display_steps(fout_, tspan[tspan < t_event], t, x, h, k, xs)
                if display_beforeevent:
                    interpolate(t, x, h, k, t_event, xs)
                    fout_(t_event, xs)
                cmd = event_callback(t_event, xs)
                if display_afterevent:
                    fout_(t_event, xs)
                if cmd == "stop":
                    if fout is None:
                        return tout, xout
                    else:
                        return None
                elif cmd == "jump":
                    F(t, x, k[0])
                    h = 0.9*hnew
                    xs, x = x, xs
                    t = t_event
                    continue
                elif cmd == "continue":
                    display_steps(fout_, tspan[tspan > t_event], t, xp, h, k, xs)
                    if display_intermediatesteps:
                        fout_(t, xp)
                    xp, x = x, xp
                    k[0], k[-1] = k[-1], k[0]
                    t = t + h
                else:
                    raise Exception("Unrecognized event command.")
            else:
                display_steps(fout_, tspan, t, x, h, k, xs)
                if t + h < tfinal:
                    if display_intermediatesteps:
                        fout_(t + h, xp)
                xp, x = x, xp
                k[0], k[-1] = k[-1], k[0]
                t = t + h
        h = hnew
    if fout is None:
        return tout, xout
    else:
        return None
