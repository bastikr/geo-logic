svg_template = \
r"""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ev="http://www.w3.org/2001/xml-events"
     version="1.1" baseProfile="full" width="600mm" height="600mm">
{elements}
</svg>
"""


def document(elements):
    return svg_template.format(elements=elements)


def draw_contourline(contour):
    svgpath = "{segments}"
    segments = []
    for path in contour:
        points = " ".join(["{x},{y}".format(x=100*x, y=100*y) for x, y in zip(path.x, path.y)])
        segment = '<polyline points="{points}" style="fill:none;stroke:black;stroke-width:0.1"/>'.format(points=points)
        segments.append(segment)
    return svgpath.format(segments="".join(segments))

def draw_contourlines(contourlines):
    return "\n".join([draw_contourline(contourline) for contourline in contourlines])


def path2svg(path, color="black"):
    # points = ['<path d="M{x},{y}'.format(x=path.x[0], y=path.y[0])]
    points = ['<path d="M{},{}'.format(*path.points[0])]
    if path.degree == 1:
        points.append("L")
    elif path.degree == 2:
        points.append("Q")
    for p in path.points[1:]:
        points.append("{},{}".format(*p))
    if path.closed:
        points.append("Z")
    #points.append(r'" fill:"none" stroke-width:"0.5"')
    points.append(r'" style="fill:none;stroke:{color};stroke-width:1"'.format(color=color))
    points.append(r"/>")
    return " ".join(points)
