import numpy
import heightmodel_bicubic
import pylab
import mpl_backend

hmap = numpy.array([
    [-1., -2, 1, 0],
    [-2, 1, 0.7, 1],
    [-1, -1, 0.5, -1],
    [-4, 1, 1, 2],
    ])

hmap = numpy.array([
    [12.251, 12.023, 12.303, 11.879],
    [5.89, 6.08, 6.318, 5.422],
    [-0.26, -0.137, 0.133, -0.569],
    [-6.649, -6.019, -5.796, -6.994],
    ])



height = 0.

path_ode1 = heightmodel_bicubic.contour(height, hmap, dh_max=None, dl_max=0.1, degree=1)
path_ode2 = heightmodel_bicubic.contour(height, hmap, dh_max=None, dl_max=0.1, degree=2)
# path = heightmodel_bicubic.contour(height, hmap, dh_max=None, dl_max=None, degree=1)

f_bicubic = heightmodel_bicubic.interpolate(hmap, [0, 1], [0, 1])

Nx = hmap.shape[0]-3
Ny = hmap.shape[1]-3
p_ode1 = path_ode1[0].scale(Nx, Ny)
p_ode2 = path_ode2[0].scale(Nx, Ny)

for path in path_ode2:
    for p in path.points:
        print(f_bicubic(*p))

# for i in range(len(p_ode.x) - 1):
#     print("Height: ", f(p_ode.x[i], p_ode.y[i]))
#     print(numpy.sqrt((p_ode.x[i+1]-p_ode.x[i])**2 + (p_ode.y[i+1]-p_ode.y[i])**2))


import paths
p_ode1_simplified = paths.simplify(f_bicubic, p_ode1, [-0.05, 0.05], 0.1)
p_ode2_simplified = paths.simplify(f_bicubic, p_ode2, [-0.05, 0.05], 0.1)

mpl_backend.plot_path(p_ode1, "k", alpha=0.2)
mpl_backend.plot_path(p_ode1_simplified, "bo", alpha=0.2)
mpl_backend.plot_path(p_ode2_simplified, "go", alpha=0.2)
mpl_backend.plot_path(p_ode1_simplified, "b", alpha=0.5)
mpl_backend.plot_path(p_ode2_simplified, "g", alpha=0.5)
pylab.xlim(-1, 2)
pylab.ylim(-1, 2)
for i in range(0, 1):
    pylab.plot([i , i], [-1, 2], color="grey")
    pylab.plot([-1, 2], [i , i], color="grey")
pylab.show()