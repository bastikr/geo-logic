import numpy

srcpath = "./data/ibk_10m/ibk_10m_float.asc"
#srcpath = "./data/sz_10m/sz_10m_float.asc"

#f = open(srcpath)
#print([f.read(400)])

def _load_property(name, f):
    line = f.readline()
    assert line.startswith(name+" "), (name, line)
    return line[len(name)+1:-1].strip()

def load(path):
    f = open(path)
    ncols = int(_load_property("NCOLS", f))
    nrows = int(_load_property("NROWS", f))
    xllcorner = float(_load_property("XLLCORNER", f))
    yllcorner = float(_load_property("YLLCORNER", f))
    cellsize = float(_load_property("CELLSIZE", f))
    nodata_value = _load_property("NODATA_VALUE", f)
    data = numpy.empty(nrows*ncols, dtype=float)
    def convert(x):
        if x == nodata_value:
            return numpy.NaN
        else:
            return float(x)
    i = 0
    for line in f:
        row = line.split(" ")[:-1]
        n = len(row)
        data[i:i+n] = map(convert, row)
        i += n
    f.close()
    return data.reshape((nrows, ncols))

data = load(srcpath)
#data = data[300:600, 300:600]
#data = data[502:517, 540:555]
#data = data[580:595, 580:595]
data = data[550:600, 550:600]
print("Shape: {}".format(data.shape))
print("Elements: {}".format(numpy.prod(data.shape)))
N_nans = numpy.isnan(data).sum()
print("# NaN: ", N_nans)
print("Height bounds: {} - {}".format(numpy.nanmin(data), numpy.nanmax(data)))

import heightmodel_nearest
import heightmodel_triangular
import heightmodel_bilinear

import heightmodel_bicubic
import paths
# import heightmodel_bicubic2 as heightmodel_bicubic
# import paths2 as paths

# hcontours = [1900., 12000., 2100., 2200.]
# hcontours = [2200.]
# hcontours = range(0, 3000, 50)
# hcontours = range(1800, 2500, 10)
hcontours = range(0, 3000, 100)
herr = 1.
herr2 = 0.03

Nx = data.shape[0] - 3
Ny = data.shape[1] - 3
f_bicubic = heightmodel_bicubic.interpolate(data, [0, Nx], [0, Ny])

import time
import cProfile

t0 = time.time()
tree_triangular = heightmodel_triangular.create_tree(data[1:-1, 1:-1])
print("Triangular tree:", time.time() - t0)

t0 = time.time()
tree_bicubic = heightmodel_bicubic.create_tree(data)
print("Bicubic tree:", time.time() - t0)

f_triangular = heightmodel_triangular.interpolate(data[1:-1, 1:-1], [0, Nx], [0, Ny])

contourlines_bicubic = []
contourlines_bicubic_simplified = []
contourlines_bicubic_simplified2 = []

t0 = time.time()
for h in hcontours:
    print("Contour for h={}".format(h))
    # P = tuple(p for p in heightmodel_bicubic.contour(h, data, dl_max=0.2, degree=2))
    P = tuple(p for p in heightmodel_bicubic.contour_tree(h, tree_bicubic, dl_max=0.2, degree=1))
    contourlines_bicubic.append(P)
    P_simplified = tuple(paths.simplify(f_bicubic, p, [h-herr, h+herr], 0.1) for p in P)
    contourlines_bicubic_simplified.append(P_simplified)
    # temp = []
    # for p in P_simplified:
    #     try:
    #         temp.append(paths.simplify_extensive(f_bicubic, p, h, h-herr2, h+herr2, c_bend=2))
    #     except IndexError:
    #         pass
    # P_simplified2 = tuple(temp)
    # contourlines_bicubic_simplified2.append(P_simplified2)
print(time.time() - t0)

# for h, contour in zip(hcontours, contourlines_bicubic):
#     for path in contour:
#         for p in path.points[::2]:
#             assert abs(f_bicubic(*p) - h) < 1e-4
# assert False

# print(contourlines_bicubic_simplified)
# print("Bending energy: ", paths.bending_energy(contourlines_bicubic[0][0]))
# print("Bending energy: ", paths.bending_energy(contourlines_bicubic_simplified[0][0]))
# print("Height energy: ", paths.height_energy(f_bicubic, contourlines_bicubic[0][0], 1905, 1904, 1906))
# print("Height energy: ", paths.height_energy(f_bicubic, contourlines_bicubic_simplified[0][0], 1905, 1904, 1906))
# print("Repulsive energy: ", paths.repulsive_energy(contourlines_bicubic[0][0]))
# print("Repulsive energy: ", paths.repulsive_energy(contourlines_bicubic_simplified[0][0]))

#p_simplified = contourlines_bicubic_simplified[0][0]


#paths.check_path(contourlines_bicubic[0][0])
#paths.check_path(p_simplified)

#h = hcontours[0]
#herr = 0.05
#p = paths.simplify_extensive(f_bicubic, p_simplified, h, h-herr, h+herr, c_bend=2)


# print(p.x)
# print(p.y)


# contourlines_bilinear = []
# for h in hcontours:
#     contourlines_bilinear.append(heightmodel_bilinear.contour(h, data[1:-1,1:-1], dl_max=0.1, degree=1))

t0 = time.time()
contourlines_triangular = []
contourlines_triangular_simplified = []
for h in hcontours:
    print("Contour for h={}".format(h))
    # C = heightmodel_triangular.contour(h, data[1:-1, 1:-1], degree=1)
    C = heightmodel_triangular.contour_tree(h, tree_triangular, degree=1)
    contourlines_triangular.append(C)
    C_simplified = tuple(paths.simplify(f_triangular, p, [h-herr, h+herr], 0.1) for p in C)
    contourlines_triangular_simplified.append(C_simplified)
print(time.time() - t0)


# assert False
# contourlines_nearest= []
# for h in hcontours:
#     contourlines_nearest.append(heightmodel_nearest.contour(h, data[1:-1,1:-1], degree=1))


#print("data.shape: ", data.shape)
#print("data[1:-1, 1:-1].shape: ", data[1:-1, 1:-1].shape)

grid = []
Nx, Ny = data[1:-1, 1:-1].shape
for i in range(Nx):
    x = i/float(Nx-1)
    grid.append(paths.Path([numpy.array([x, 0.]), numpy.array([x, 1.])]))

for j in range(Ny):
    y = j/float(Ny-1)
    grid.append(paths.Path([numpy.array([0., y]), numpy.array([1., y])]))


import pylab
import mpl_backend
import svg_backend


svg_paths = []
# svg_paths.append(svg_backend.path2svg(p.scale(100, 100), color="red"))
# for path in grid:
#     svg_paths.append(svg_backend.path2svg(path.scale((Nx-1)*100, (Ny-1)*100), color="black"))

# for contourline in contourlines_bicubic:
#     for path in contourline:
# #         # pylab.plot(path.x[::2], path.y[::2], "ob", ms=6, alpha=0.5)
# #         # pylab.plot(path.x[1::2], path.y[1::2], "or", ms=6, alpha=0.5)
# #         # mpl_backend.plot_path(path, "b", alpha=0.5)
#         svg_paths.append(svg_backend.path2svg(path.scale(100, 100), color="blue"))

for contourline in contourlines_bicubic_simplified:
    for path in contourline:
        # pylab.plot(path.x[::2], path.y[::2], "og", ms=6, alpha=0.5)
        # pylab.plot(path.x[1::2], path.y[1::2], "xg", ms=6, alpha=0.5)
        # mpl_backend.plot_path(path, "g", alpha=0.5)
        svg_paths.append(svg_backend.path2svg(path.scale(100, 100), color="green"))

# for contourline in contourlines_bicubic_simplified2:
#     for path in contourline:
#         # pylab.plot(path.x, path.y, "ob", ms=3)
#         # mpl_backend.plot_path(path, "b", alpha=0.5)
#         svg_paths.append(svg_backend.path2svg(path.scale(100, 100), color="orange"))

# for contourline in contourlines_bilinear:
#     for path in contourline:
#         # pylab.plot(path.x, path.y, "ob", ms=3)
#         # mpl_backend.plot_path(path, "b", alpha=0.5)
#         svg_paths.append(svg_backend.path2svg(path.scale(600, 600), color="green"))


# for contourline in contourlines_triangular:
#     for path in contourline:
#         # pylab.plot(path.x, path.y, "ob", ms=3)
#         # mpl_backend.plot_path(path, "b", alpha=0.5)
#         svg_paths.append(svg_backend.path2svg(path.scale(100, 100), color="orange"))


for contourline in contourlines_triangular_simplified:
    for path in contourline:
        # pylab.plot(path.x, path.y, "ob", ms=3)
        # mpl_backend.plot_path(path, "b", alpha=0.5)
        svg_paths.append(svg_backend.path2svg(path.scale(100, 100), color="violet"))

# for contourline in contourlines_nearest:
#     for path in contourline:
#         # pylab.plot(path.x, path.y, "ob", ms=3)
#         # mpl_backend.plot_path(path, "b", alpha=0.5)
#         svg_paths.append(svg_backend.path2svg(path.scale(100, 100), color="orange"))

#svg_paths = svg_backend.draw_contourlines(contourlines)


# for contourline in contourlines_bezier:
#     for path in contourline:
#         mpl_backend.plot_path(path, "r", alpha=0.5)
#         svg_paths.append(svg_backend.path2svg(path.scale(600, 600), color="red"))
#         pylab.plot(path.x[1::2], path.y[1::2], "ow")
#         pylab.plot(path.x[::2], path.y[::2], "or")

document = svg_backend.document("\n".join(svg_paths))
f = open("test.svg", "w")
f.write(document)
f.close()

# pylab.show()
