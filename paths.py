import numpy
import scipy.integrate
import scipy.optimize

class Path:
    """
    Defines a path through multiple points.

    The points have a different interpretation depending on the degree
    parameter.
    * Degree=1
        All points are connected by straight lines.

    * Degree=2:
        The points define a quadratic bezier curve. Every second point is a
        control point and in general is not on the path itself.

    * Degree=N:
        N-th order bezier curve.

    **Arguments**
        *x*
            Numpy array defining the x-coordinates.

        *y*
            Numpy array defining the y-coordinates.

        *degree*
            Integer defining the degree of the spline polynomial.

        *closed*
            Boolean determining if the last point and the first point are
            connected.
    """

    def __init__(self, points, degree=1, closed=False):
        assert isinstance(points, list)
        assert isinstance(degree, int) and 0 < degree
        assert isinstance(closed, bool)
        self.points = points
        self.degree = degree
        self.closed = closed

    def __iter__(self):
        return SegmentIterator(self)

    def shift(self, xshift, yshift):
        dp = numpy.array([xshift, yshift], dtype=float)
        return Path(list(p + dp for p in self.points),
                    degree=self.degree, closed=self.closed)

    def scale(self, xscale, yscale):
        s = numpy.array([xscale, yscale], dtype=float)
        return Path(list(s*p for p in self.points),
                    degree=self.degree, closed=self.closed)

    def rotate(self, alpha, origin=None):
        c = numpy.cos(alpha)
        s = numpy.sin(alpha)
        R = numpy.array([[c, -s], [s, c]])
        if origin is None:
            points = (R.dot(p) for p in self.points)
        else:
            points = (origin + (R - origin).dot(p) for p in self.points)
        return Path(list(points), degree=self.degree, closed=self.closed)

    def reverse(self):
        if self.degree > 1:
            raise NotImplementedError()
        return Path(self.points[::-1], degree=self.degree, closed=self.closed)

    def copy(self):
        return Path(list(self.points), degree=self.degree, closed=self.closed)


class SegmentIterator:
    def __init__(self, path):
        self.path = path
        self.degree = path.degree
        self.index = 0
        self.N = len(path.points)

    def next(self):
        if self.index >= self.N-1:
            raise StopIteration()
        nextindex = self.index + self.degree
        points = self.path.points[self.index:nextindex+1]
        self.index = nextindex
        return Path(points, degree=self.degree, closed=False)


def bending_energy(path):
    assert path.degree == 2
    sqrnorm = lambda x:x.dot(x)
    norm = numpy.linalg.norm
    energy = 0
    for segment in path:
        P0 = numpy.array([segment.x[0], segment.y[0]])
        P1 = numpy.array([segment.x[1], segment.y[1]])
        P2 = numpy.array([segment.x[2], segment.y[2]])
        db0 = P1-P0
        db1 = P2-P1
        db2 = P0-P2

        def Lambda(alpha):
            return alpha*sqrnorm(db0) + (1.-alpha)*sqrnorm(db1) - 1./4*sqrnorm(db2)

        l14 = Lambda(1./4)
        l34 = Lambda(3./4)
        l12 = Lambda(1./2)

        if numpy.abs(numpy.cross(db0, db1))/(norm(db0)*norm(db1)) < 1e-15:
            continue
        c = 2./(3*numpy.cross(db0, db1)**2)
        A = l14*(3*l12*sqrnorm(db1) - l14**2)/norm(db1)**3
        B = l34*(3*l12*sqrnorm(db0) - l34**2)/norm(db0)**3
        energy += c*(A + B)
    return energy


def length(path):
    norm = numpy.linalg.norm
    sqrnorm = lambda x:x.dot(x)
    if path.degree == 1:
        L = 0.
        for segment in path:
            L += numpy.linalg.norm(segment.points[-1] - segment.points[0])
    elif path.degree == 2:
        L = 0.
        for segment in path:
            db0 = segment.points[1] - segment.points[0]
            db1 = segment.points[2] - segment.points[1]
            db2 = segment.points[0] - segment.points[2]

            if numpy.abs(numpy.cross(db0, db1))/(norm(db0)*norm(db1)) < 1e-15:
                L += norm(db2)
                continue

            sqrnorm = lambda x:x.dot(x)
            norm = numpy.linalg.norm

            def Lambda(alpha):
                return alpha*sqrnorm(db0) + (1.-alpha)*sqrnorm(db1) - 1./4*sqrnorm(db2)

            l14 = Lambda(1./4)
            l34 = Lambda(3./4)
            l12 = Lambda(1./2)

            a = (l14*norm(db1) + l34*norm(db0))/(2*l12)
            b = numpy.cross(db0, db1)**2/(8*l12**(3./2))
            c = numpy.log(l14 + norm(db1)*numpy.sqrt(l12))
            d = numpy.log(-l34 + norm(db0)*numpy.sqrt(l12))

            L += a + b*(c-d)
    else:
        NotImplementedError()
    return L


def lineintegral(path, f):
    I = 0
    for segment in path:
        P0 = numpy.array([segment.x[0], segment.y[0]])
        P1 = numpy.array([segment.x[1], segment.y[1]])
        P2 = numpy.array([segment.x[2], segment.y[2]])
        def T(t, P0, P1, P2):
            return -2*(1-t)*P0 + (2-4*t)*P1 + 2*t*P2
        def f_(t):
            dl = numpy.linalg.norm(T(t, P0, P1, P2))
            p = eval_bezier_degree2(t, P0, P1, P2)
            return f(p)*dl
        I += scipy.integrate.quad(f_, 0, 1)[0]
    return I


def height_energy(f_height, path, h0, hmin, hmax):
    def weight(h, h0, hmin, hmax):
        dh = h-h0
        return 0.5*(numpy.exp(dh/(hmax-h0)) + numpy.exp(dh/(hmin-h0)))**4
    def f(p):
        h = f_height(*p)
        return weight(h, h0, hmin, hmax)
    try:
        return lineintegral(path, f)
    except ValueError:
        return numpy.inf


def repulsive_energy(path):
    lspan = tuple(length(p) for p in path)
    L = sum(lspan)
    l0 = L/(len(lspan) - 1)
    return l0*sum(1./l for l in lspan)


def energy(f, path, h0, hmin, hmax, c_bend=1, c_height=1, c_repulsive=1):
    E_bend = bending_energy(path)
    E_height = height_energy(f, path, h0, hmin, hmax)
    E_repulsive = repulsive_energy(path)
    return c_bend*E_bend + c_height*E_height + c_repulsive*E_repulsive

# P = Path([0,5,2], [1,1,10], degree=2, closed=False)
# print(bending_energy(P))
# print(length(P))
# print(lineintegral(P, lambda x: 1))


# def is_identical(path1, path2, index1, index2, eps=None):
#     if eps is None:
#         return (path1.x[index1] == path2.x[index2]) and \
#                (path1.y[index1] == path2.y[index2])
#     else:
#         return numpy.abs(path1.x[index1] - path2.x[index2]) < eps and \
#                numpy.abs(path1.y[index1] - path2.y[index2]) < eps


def is_identical(path1, path2, index1, index2, eps=None):
    point1 = path1.points[index1]
    point2 = path2.points[index2]
    if eps is None:
        return (point1 == point2).all()
    else:
        return numpy.linalg.norm(point2 - point1) < eps
        # return (numpy.abs(point2 - point1) < eps).all()


def connect(path1, path2, eps=None):
    points1 = path1.points
    points2 = path2.points
    if is_identical(path1, path2, 0, 0, eps=eps):
        points = points1[::-1] + points2[1:]
    elif is_identical(path1, path2, 0, -1, eps=eps):
        points = points2 + points1[1:]
    elif is_identical(path1, path2, -1, 0, eps=eps):
        points = points1 + points2[1:]
    elif is_identical(path1, path2, -1, -1, eps=eps):
        points = points1 + points2[-2::-1]
    else:
        return None
    return Path(points, degree=path1.degree, closed=False)


def close_path(path, eps=None):
    if is_identical(path, path, 0, -1, eps):
        p = path.copy()
        p.points[-1] = p.points[0]
        p.closed = True
        return p
    else:
        return None


def is_identical_path(path1, path2, eps=1e-10):
    if path1.closed or path2.closed:
        raise NotImplementedError()
    if len(path1.points) != len(path2.points):
        return False
    for i in range(len(path1.points)):
        if not is_identical(path1, path2, i, i, eps=eps):
            return False
    return True


def remove_duplicates(paths, eps=1e-10):
    unique_paths = []
    for path in paths:
        path_reversed = path.reverse()
        for unique_path in unique_paths:
            if is_identical_path(unique_path, path, eps=eps) or\
                    is_identical_path(unique_path, path_reversed, eps=eps):
                break
        else:
            unique_paths.append(path)
    return unique_paths


def merge_paths(paths, eps=1e-10):
    paths_merged = []
    for path in paths:
        paths_merged.append(path.copy())

    N = len(paths_merged)
    i = 0
    while i < N:
        path = paths_merged[i]
        if path.closed:
            i += 1
            continue
        p = close_path(path, eps)
        if p is not None:
            paths_merged[i] = p
            i += 1
            continue

        for j in range(i+1, N):
            newpath = connect(path, paths_merged[j], eps=eps)
            if newpath is not None:
                paths_merged[i] = newpath
                del paths_merged[j]
                N -= 1
                break
        else:
            i += 1
    return paths_merged


def _inbounds_degree1(f, path, hlim, dl_max):
    assert path.degree == 1
    for i in range(len(path.points)-1):
        dp = path.points[i+1] - path.points[i]
        dl = numpy.linalg.norm(dp)
        N = int(dl/dl_max) + 1
        dp_ = dp/N
        for j in range(N-1):
            p_ = path.points[i] + dp_*(j+1)
            if not hlim[0] <= f(*p_) <= hlim[1]:
                return False
    return True


def eval_bezier_degree2(t, p0, p1, p2):
    return (1 - t)**2*p0 + 2*(1-t)*t*p1 + t**2*p2


def _inbounds_degree2(f, path, hlim, dl_max):
    assert path.degree == 2
    for i in range(0, len(path.points)-2, 2):
        dp0 = path.points[i+1] - path.points[i]
        dp1 = path.points[i+2] - path.points[i+1]
        dl = numpy.linalg.norm(dp0) + numpy.linalg.norm(dp1)
        N = int(dl/dl_max) + 1
        for j in range(1, N-1):
            p_ = eval_bezier_degree2(j/float(N), *path.points[i:i+3])
            try:
                if not hlim[0] <= f(*p_) <= hlim[1]:
                    return False
            except ValueError:
                return False
    return True


def inbounds(f, path, hlim, dl_max):
    if path.degree == 1:
        return _inbounds_degree1(f, path, hlim, dl_max)
    elif path.degree == 2:
        for segment in path:
            if not _inbounds_degree2(f, segment, hlim, dl_max):
                return False
        return True


def _simplify_degree1(f, path, hlim, dl_max):
    assert path.degree == 1
    P0, P1 = path.points[:2]
    newpath = [P0]
    for i in range(2, len(path.points)):
        P1_candidate = path.points[i]
        segment = Path([P0, P1_candidate], degree=1, closed=False)
        if not inbounds(f, segment, hlim, dl_max):
            newpath.append(P1)
            P0 = P1
        P1 = P1_candidate
    newpath.append(path.points[-1])
    return Path(newpath, degree=path.degree, closed=path.closed)


def intersect_lines(p0, u0, p1, u1):
    c = 1./(u1[0]*u0[1] - u1[1]*u0[0])
    t = c*(p1[1] - p0[1])*u0[0] - c*(p1[0] - p0[0])*u0[1]
    px = numpy.array((p1[0] + t*u1[0], p1[1] + t*u1[1]))
    norm = numpy.linalg.norm
    v = [p1[0] - p0[0], p1[1] - p0[1]]
    angle1 = numpy.dot(u0, u1)/(norm(u0)*norm(u1))
    angle2 = numpy.dot(u0, v)/(norm(u0)*norm(v))
    if numpy.arccos(angle1) < 1e-2 and numpy.arccos(angle2) < 1e-2:
        px = numpy.array(((p0[0] + p1[0])/2., (p0[1] + p1[1])/2.))
    return px


def _angle(x, y):
    cosa = numpy.dot(x, y)/(numpy.linalg.norm(x)*numpy.linalg.norm(y))
    return 1 - abs(cosa)


def _simplify_degree2(f, path, hlim, dl_max):
    assert path.degree == 2
    P0, C, P1 = path.points[:3]
    u0 = C - P0
    u1 = P1 - C
    newpath = [P0]
    for i in range(4, len(path.points), 2):
        P1_candidate = path.points[i]
        u1_candidate = P1_candidate - path.points[i-1]
        C_candidate = intersect_lines(P0, u0, P1_candidate, u1_candidate)
        dp = P1_candidate - P0
        if _angle(u0, dp) < 1e-6 and _angle(u1_candidate, dp) < 1e-6:
            C_candidate = (P0 + P1_candidate)/2.
        r1 = P1_candidate - P0
        r2 = C_candidate - P0
        segment = Path([P0, C_candidate, P1_candidate], degree=2, closed=False)
        if 0.1 < numpy.dot(r1, r2)/numpy.dot(r1, r1) < 0.9\
                and inbounds(f, segment, hlim, dl_max)\
                and numpy.dot(r2, r2) < numpy.dot(r1, r1):
            C = C_candidate
        else:
            newpath.append(C)
            newpath.append(P1)
            P0 = P1
            u0 = u1
            C = path.points[i-1]
        P1 = P1_candidate
        u1 = u1_candidate
    newpath.append(C)
    newpath.append(P1)
    return Path(newpath, degree=path.degree, closed=path.closed)


def simplify_extensive(f_height, path, h0, hmin, hmax, c_bend=1, c_height=1, c_repulsive=0.1):
    # N = len(path.x)
    # N_control = (N-1)/2

    def vec2path(u):
        N_control = len(u)/3
        N = 2*N_control + 1
        x_vec = numpy.empty((2*N_control+1,))
        y_vec = numpy.empty((2*N_control+1,))
        x_vec[1::2] = u[:N_control]
        y_vec[1::2] = u[N_control:2*N_control]
        C0 = numpy.array([x_vec[-2], y_vec[-2]])
        C1 = numpy.array([x_vec[1], y_vec[1]])
        P = C0 + u[2*N_control]*(C1 - C0)/numpy.linalg.norm(C1 - C0)
        x_vec[0] = x_vec[-1] = P[0]
        y_vec[0] = y_vec[-1] = P[1]
        for i in range(1, N-N_control-1):
            C0 = numpy.array([x_vec[2*i-1], y_vec[2*i-1]])
            C1 = numpy.array([x_vec[2*i+1], y_vec[2*i+1]])
            P = C0 + u[2*N_control+i]*(C1 - C0)/numpy.linalg.norm(C1 - C0)
            x_vec[2*i] = P[0]
            y_vec[2*i] = P[1]
        return Path(x_vec, y_vec, degree=2, closed=True)

    def path2vec(p):
        N = len(p.x)
        N_control = (N-1)/2
        u_vec = numpy.empty((2*N_control + (N - N_control - 1),))
        u_vec[:N_control] = p.x[1::2]
        u_vec[N_control:2*N_control] = p.y[1::2]
        C0 = numpy.array([p.x[-2], p.y[-2]])
        P = numpy.array([p.x[0], p.y[0]])
        u_vec[2*N_control] = numpy.linalg.norm(P-C0)
        for i in range(1, N-N_control-1):
            C0 = numpy.array([p.x[2*i-1], p.y[2*i-1]])
            P = numpy.array([p.x[2*i], p.y[2*i]])
            u_vec[2*N_control+i] = numpy.linalg.norm(P-C0)
        return u_vec

    def _repulsive_energy(path):
        lspan = list(length(p) for p in path)
        L = sum(lspan)
        l0 = L/(len(lspan) - 1)
        imin = numpy.argmin(lspan)
        lmin = lspan[imin]
        lspan[imin] = numpy.inf
        print("l: ", lmin)
        return l0*sum(1./l for l in lspan) + 10*lmin

    def _energy_removepoints(u):
        # E = energy(f_height, vec2path(u), h0, hmin, hmax, c_bend, c_height, c_repulsive)
        p = vec2path(u)
        E = energy(f_height, p, h0, hmin, hmax, c_bend, c_height, 0)
        E += _repulsive_energy(p)
        print(E)
        return E

    def _energy(u):
        E = energy(f_height, vec2path(u), h0, hmin, hmax, c_bend, c_height, c_repulsive)
        print(E)
        return E

    while inbounds(f_height, path, [hmin, hmax], 0.1):
        N = len(path.x)
        N_control = (N-1)/2
        oldpath = path
        u_vec = scipy.optimize.fmin(_energy_removepoints, path2vec(path), ftol=5, xtol=10)
        lspan = list(length(p) for p in path)
        i = numpy.argmin(lspan)
        u_new = numpy.empty((2*(N_control-1) + (N - N_control - 2),))
        u_new[:i] = u_vec[:i]
        u_new[i:N_control+i-1] = u_vec[i+1:N_control+i]
        u_new[N_control+i-1:2*N_control+i-2] = u_vec[N_control+i+1:2*N_control+i]
        u_new[2*N_control+i-2:] = u_vec[2*N_control+i+1:]
        path = vec2path(u_new)
    u_vec = scipy.optimize.fmin(_energy, path2vec(oldpath), ftol=1, xtol=10)
    return vec2path(u_vec)


def simplify(f, path, hlim, dl_max):
    if path.degree == 1:
        return _simplify_degree1(f, path, hlim, dl_max)
    elif path.degree == 2:
        return _simplify_degree2(f, path, hlim, dl_max)
    else:
        raise NotImplementedError()


def check_path(path):
    N = len(path.points)
    for i in range(0, N-2, 2):
        P0 = path.points[i]
        C = path.points[i+1]
        P1 = path.points[i+2]
        d0 = C - P0
        d1 = P1 - P0
        l0 = numpy.linalg.norm(d0)
        l1 = numpy.linalg.norm(d1)
        #assert numpy.cross(d0, d1)/(l0*l1) < 1e-2
        assert 0 < d1.dot(d0)/l1 < 1
