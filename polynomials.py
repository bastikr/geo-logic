import numpy
import scipy.special

u1 = 1.
u2 = (-1+1j*3**0.5)/2
u3 = (-1-1j*3**0.5)/2


def roots_cubic(d, c, b, a):
    """
    Calculate real valued roots of the equation a*x**3 + b*x**2 + c*x + d = 0
    """
    Delta = 18*a*b*c*d - 4*b**3*d + b**2*c**2 - 4*a*c**3 - 27*a**2*d**2
    Delta0 = b**2 - 3*a*c

    # Handle special case Delta==0 first
    if Delta == 0:
        if Delta0 == 0:
            # Triple, real valued root
            return (-b/(3.*a),)
        else:
            # 1 real valued root and a double, real valued root
            x1 = (9.*a*d - b*c)/(2.*Delta0)
            x3 = (4.*a*b*c - 9.*a**2*d - b**3)/(a*Delta0)
            if x1 > x3:
                return (x3, x1)
            else:
                return (x1, x3)

    # Handle generic case Delta!=0
    Delta1 = 2*b**3 - 9*a*b*c + 27*a**2*d
    if Delta0 == 0:
        C = Delta1**(1./3.)
    else:
        C = ((Delta1 + (Delta1**2 - 4*Delta0**3+0j)**0.5)/2.)**(1./3.)
    x1 = -1./(3*a)*(b + u1*C + Delta0/(u1*C))
    x2 = -1./(3*a)*(b + u2*C + Delta0/(u2*C))
    x3 = -1./(3*a)*(b + u3*C + Delta0/(u3*C))
    solutions = [x1, x2, x3]
    if Delta > 0:
        # Polynomial has 3 real valued roots
        return tuple(sorted(x.real for x in solutions))
    elif Delta < 0:
        # Polynomial has 1 real valued root and two complex roots
        ind = numpy.abs(numpy.imag(solutions)).argmin()
        return (solutions[ind].real,)


def roots_quadratic(c, b, a):
    """
    Calculate real valued roots of the equation a*x**2 + b*x + c = 0
    """
    eps = 1e-12
    discriminant = b**2 - 4*a*c
    if -eps < discriminant < eps:
        roots = [-b/(2*a)]
    elif discriminant < 0:
        roots = []
    else:
        assert discriminant > 0
        k = discriminant**0.5
        roots = [-b-k, -b+k]
    return tuple(roots)


def roots_linear(b, a):
    """
    Calculate real valued root of the equation a*x + b = 0
    """
    return (-b/a,)


def roots(coeffs, eps=1e-15):
    """
    Calculate all real valued roots of the given polynomial.

    Only implemented for polynomials of degree <= 3.

    **Arguments**
        *coeffs*
                List of N+1 coefficients [a_N, ..., a_1, a_0] specifying the
                polynom of order N, \sum_{i=0}^N a_i^i.

    **Returns**
        Ordered tuple of real numbers.
    """
    r = []
    for root in numpy.roots(coeffs[::-1]):
        if numpy.abs(root.imag) < eps:
            r.append(root.real)
    return r
    N = len(coeffs) - 1
    assert 0 <= N <= 3
    if N == 0:
        if not -eps < coeffs[0] < +eps:
            return []
        else:
            raise Exception("Infinitely many intersections.")

    if -eps < coeffs[-1] < eps:
        return roots(coeffs[:-1], eps=eps)

    if N == 1:
        return roots_linear(*coeffs)
    elif N == 2:
        return roots_quadratic(*coeffs)
    elif N == 3:
        return roots_cubic(*coeffs)


def powervector(u):
    return numpy.array([u**n for n in range(4)])


def dpowervector(u):
    return numpy.array([n*u**(n-1) if n > 0 else 0 for n in range(4)])


def powervector_derivative(u, derivative):
    fac_d = numpy.math.factorial(derivative)
    binom = scipy.special.binom
    V = []
    for n in range(-1, 3, 1):
        if n >= derivative:
            V.append(fac_d*binom(n, derivative)*u**(n - derivative))
        else:
            V.append(0.)
    return numpy.array(V)


# Basis points for lagrange polynomial
basispoints = (-1, 0, 1, 2)

# Transformation matrices between polynomials in standard, lagrange and
# bezier representation.
T_standard2lagrange = numpy.array([powervector(x) for x in basispoints])
# T_lagrange2standard = 1./6*numpy.array([
#     [-1,  3, -3,  1],
#     [+3, -6,  3,  0],
#     [-2, -3,  6, -1],
#     [+0,  6,  0,  0]
#     ])[::-1, :]
T_lagrange2standard = numpy.linalg.inv(T_standard2lagrange)

T_bezier2standard = numpy.array([
    [-1,  3, -3,  1],
    [+3, -6,  3,  0],
    [-3,  3,  0,  0],
    [+1,  0,  0,  0]
    ])
T_bezier2standard = T_bezier2standard[::-1, :]
T_standard2bezier = numpy.linalg.inv(T_bezier2standard)

T_bezier2lagrange = T_standard2lagrange.dot(T_bezier2standard)
T_lagrange2bezier = T_standard2bezier.dot(T_lagrange2standard)

T_standard2hermite = numpy.array([
    [1, 0, 0, 0],
    [1, 1, 1, 1],
    [0, 1, 0, 0],
    [0, 1, 2, 3],
    ], dtype=float)
T_hermite2standard = numpy.linalg.inv(T_standard2hermite)

T_hpatch2hermite = numpy.array([
    [0, 1, 0, 0],
    [0, 0, 1, 0],
    [-0.5, 0, 0.5, 0],
    [0, -0.5, 0, 0.5],
    ], dtype=float)
T_hermite2hpatch = numpy.linalg.inv(T_hpatch2hermite)

T_hpatch2standard = T_hermite2standard.dot(T_hpatch2hermite)
T_standard2hpatch = T_hermite2hpatch.dot(T_standard2hermite)

T_hpatch2bezier = T_standard2bezier.dot(T_hpatch2standard)
T_bezier2hpatch = T_standard2hpatch.dot(T_bezier2standard)

T_lagrange2hpatch = T_standard2hpatch.dot(T_lagrange2standard)
T_hpatch2lagrange = T_standard2lagrange.dot(T_hpatch2standard)


def lagrange2standard(coeffs_lagrange):
    """
    Converts coefficients from representation at unit points to standard.

    **Arguments**
        *coeffs_lagrange*
            Values of the polynomial at the points x={-1, 0, 1, 2}.

    **Returns**
        List of N+1 coefficients [a_N, ..., a_1, a_0] specifying the
        polynom of order N, \sum_{i=0}^N a_i^i.
    """
    return T_lagrange2standard.dot(coeffs_lagrange)


def standard2lagrange(coeffs_standard):
    """
    Converts coefficients from representation at unit points to standard.

    **Arguments**
        *coeffs_standard*
            List of N+1 coefficients [a_N, ..., a_1, a_0] specifying the
            polynom of order N, \sum_{i=0}^N a_i^i.

    **Returns**
        Values of the polynomial at the points x={-1, 0, 1, 2}.
    """
    return T_standard2lagrange.dot(coeffs_standard)


def horner(coeffs, x):
    p = 0.
    for c in coeffs[::-1]:
        p = p*x + c
    return p


def horner2d(coeff_matrix, x, y):
    return horner([horner(coeffs, y) for coeffs in coeff_matrix], x)


def dhorner(coeffs, x, der):
    p = 0.
    n = len(coeffs)
    for i in range(n, der, -1):
        deriv_coeff = numpy.prod(range(i-1, i-der-1, -1))
        p = p*x + deriv_coeff*coeffs[i-1]
    return p


def dhorner2d(coeff_matrix, x, y, der_x, der_y):
    return dhorner([dhorner(coeffs, y, der_y) for coeffs in coeff_matrix], x, der_x)
