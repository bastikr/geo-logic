import matplotlib.pyplot as plt
import numpy


def plot_path(path, *args, **kwargs):
    x = []
    y = []
    for p in path.points:
        x.append(p[0])
        y.append(p[1])
    plt.plot(x, y, *args, **kwargs)
