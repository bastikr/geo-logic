import numpy
import heightmodel_triangular
import paths
import pylab
import mpl_backend

hmap = numpy.array([
    [-1., -2, 1, 0],
    [-2, 0., 0.1, 1],
    [-1, -1, 0., -1],
    [-4, 1, 1, 2],
    ])

f_triangular = heightmodel_triangular.interpolate(hmap, [0, 3], [0, 3])

h0 = 0.
dh = 0.5

contours = heightmodel_triangular.contour(h0, hmap, degree=1)
contours_simplified = tuple(paths.simplify(f_triangular, p, [h0-dh, h0+dh], 0.1) for p in contours)

for path in contours:
    for p in path.points:
        print(f_triangular(*p))

for path in contours:
    mpl_backend.plot_path(path, "k", alpha=0.2)
    mpl_backend.plot_path(path, "bo", alpha=0.2)

for path in contours_simplified:
    mpl_backend.plot_path(path, "g", alpha=0.2)
    mpl_backend.plot_path(path, "go", alpha=0.2)

pylab.xlim(0, 3)
pylab.ylim(0, 3)
for i in range(0, 4):
    pylab.plot([i, i], [0, 3], color="grey")
    pylab.plot([0, 3], [i, i], color="grey")
pylab.show()
