import numpy
import heightmodel_bilinear
import pylab
import mpl_backend

hmap = numpy.array([
    [-1., -2, 1, 0],
    [-2, 1, 0.7, 1],
    [-1, -1, 0.5, -1],
    [-4, 1, 1, 2],
    ])

height = 0.

path_ode1 = heightmodel_bilinear.contour(height, hmap, dh_max=None, dl_max=0.1, degree=1)
path_ode2 = heightmodel_bilinear.contour(height, hmap, dh_max=None, dl_max=0.1, degree=2)
# path = heightmodel_bilinear.contour(height, hmap, dh_max=None, dl_max=None, degree=1)

f = heightmodel_bilinear.interpolate(hmap, [-1, 2], [-1, 2])

p_ode1 = path_ode1[0].scale(3, 3).shift(-1, -1)
p_ode2 = path_ode2[0].scale(3, 3).shift(-1, -1)
#p_ = path[0].scale(3, 3).shift(-1, -1)

# for i in range(len(p_ode.x) - 1):
#     print("Height: ", f(p_ode.x[i], p_ode.y[i]))
#     print(numpy.sqrt((p_ode.x[i+1]-p_ode.x[i])**2 + (p_ode.y[i+1]-p_ode.y[i])**2))


import paths
p_ode1_simplified = paths.simplify(f, p_ode1, [-0.1, 0.1], 0.1)
p_ode2_simplified = paths.simplify(f, p_ode2, [-0.1, 0.1], 0.1)

mpl_backend.plot_path(p_ode1, "k", alpha=0.2)
mpl_backend.plot_path(p_ode1_simplified, "bo", alpha=0.2)
mpl_backend.plot_path(p_ode2_simplified, "go", alpha=0.2)
mpl_backend.plot_path(p_ode1_simplified, "b", alpha=0.5)
mpl_backend.plot_path(p_ode2_simplified, "g", alpha=0.5)
pylab.xlim(-1, 2)
pylab.ylim(-1, 2)
for i in range(-1, 3):
    pylab.plot([i , i], [-1, 2], color="grey")
    pylab.plot([-1, 2], [i , i], color="grey")
pylab.show()