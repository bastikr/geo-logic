import numpy
import paths
import tree


def heightbounds(hpatch):
    return hpatch.min(), hpatch.max()


def intersect_line(p0, p1):
    dx = p1[0] - p0[0]
    dy = p1[1] - p0[1]
    t = p0[2]/float(p0[2] - p1[2])
    return numpy.array([p0[0] + t*dx, p0[1] + t*dy])


def intersect_triangle(p0, p1, p2):
    P = [p0, p1, p2]
    is_below = numpy.array([p[2] < 0. for p in P])
    is_above = numpy.array([p[2] > 0. for p in P])
    is_equal = numpy.array([p[2] == 0. for p in P])
    N_below = is_below.sum()
    N_above = is_above.sum()
    N_equal = is_equal.sum()
    if N_below == 3 or N_above == 3:
        return None
    if N_equal == 0:
        if N_below == 1:
            index_alone = numpy.where(is_below)[0][0]
        else:
            index_alone = numpy.where(is_above)[0][0]
        p_alone = P[index_alone]
        p_others = tuple(P[i] for i in range(3) if i != index_alone)
        X1 = intersect_line(p_alone, p_others[0])
        X2 = intersect_line(p_alone, p_others[1])
        return paths.Path([X1, X2], degree=1, closed=False)
    if N_equal == 1:
        if N_below == 2 or N_above == 2:
            # Only intersects in a single point
            return None
        # Intersection from the equal point to opposite triangle site
        index = numpy.where(is_equal)[0][0]
        X1 = P[index][:2]
        X2 = intersect_line(*tuple(P[i] for i in range(3) if i != index))
        return paths.Path([X1, X2], degree=1, closed=False)
    if N_equal == 2:
        X1 = P[numpy.where(is_equal)[0][0]][:2]
        X2 = P[numpy.where(is_equal)[0][1]][:2]
        return paths.Path([X1, X2], degree=1, closed=False)
    if N_equal == 3:
        return None


def elementary_contour(hpatch):
    assert hpatch.shape == (2, 2)

    h_min, h_max = heightbounds(hpatch)
    if not h_min <= 0 <= h_max:
        return []

    h_center = hpatch.mean()
    p00 = numpy.array([0, 0, hpatch[0, 0]])
    p10 = numpy.array([1, 0, hpatch[1, 0]])
    p01 = numpy.array([0, 1, hpatch[0, 1]])
    p11 = numpy.array([1, 1, hpatch[1, 1]])
    p_c = numpy.array([0.5, 0.5, h_center])
    triangles = [
        [p00, p10, p_c],
        [p00, p01, p_c],
        [p11, p10, p_c],
        [p11, p01, p_c],
    ]
    intersections = [intersect_triangle(*t) for t in triangles]
    P = [p for p in intersections if p is not None]
    return paths.merge_paths(paths.remove_duplicates(P))


def contour(height, hmap, degree):
    Nx, Ny = hmap.shape
    contours = []
    for i in range(Nx-1):
        for j in range(Ny-1):
            hpatch = hmap[i:i+2, j:j+2] - height
            if numpy.isnan(hpatch).any():
                continue
            for p in elementary_contour(hpatch):
                contours.append(p.shift(i, j))
    return paths.merge_paths(contours)


def contour_tree(height, treenode, degree):
    contours = []
    if not treenode.hmin <= height <= treenode.hmax:
        pass
    elif treenode.children is None:
        patch = treenode.data
        if not numpy.isnan(patch.data).any():
            hpatch = patch.data - height
            for p in elementary_contour(hpatch):
                contours.append(p.shift(patch.i, patch.j))
    else:
        for node in treenode.children:
            contours.extend(contour_tree(height, node, degree))
        contours = paths.merge_paths(contours)
    return contours


def create_tree(hmap, i=0, j=0):
    Nx = hmap.shape[0] - 1
    Ny = hmap.shape[1] - 1
    assert Nx > 0, (Nx, Ny)
    assert Ny > 0, (Nx, Ny)
    if Nx == 1 and Ny == 1:
        if numpy.isnan(hmap).any():
            hmin = hmax = numpy.NaN
        else:
            hmin, hmax = heightbounds(hmap)
        return tree.Node(hmin, hmax, None, tree.Patch(i, j, hmap))
    if Nx >= Ny:
        nx = Nx//2
        node0 = create_tree(hmap[:nx+1, :], i=i, j=j)
        node1 = create_tree(hmap[nx:, :], i=i+nx, j=j)
    else:
        ny = Ny//2
        node0 = create_tree(hmap[:, :ny+1], i=i, j=j)
        node1 = create_tree(hmap[:, ny:], i=i, j=j+ny)
    hmin = numpy.nanmin([node0.hmin, node1.hmin])
    hmax = numpy.nanmax([node0.hmax, node1.hmax])
    return tree.Node(hmin, hmax, (node0, node1), None)


def interpolate(hmap, xlim, ylim):
    Nx, Ny = hmap.shape

    def f(x, y):
        x_ = (x - xlim[0])/float(xlim[1] - xlim[0])*(Nx-1)
        y_ = (y - ylim[0])/float(ylim[1] - ylim[0])*(Ny-1)
        i = int(x_)
        j = int(y_)
        x_ -= i
        y_ -= j
        hpatch = hmap[i:i+2, j:j+2]
        hc = hpatch.mean()
        if x_ + y_ <= 1 and x_ - y_ <= 0.:
            kx = 2*hc - (hpatch[0, 0] + hpatch[0, 1])
            ky = hpatch[0, 1] - hpatch[0, 0]
        elif x_ + y_ <= 1 and x_ - y_ > 0.:
            kx = hpatch[1, 0] - hpatch[0, 0]
            ky = 2*hc - (hpatch[0, 0] + hpatch[1, 0])
        elif x_ + y_ > 1 and x_ - y_ <= 0.:
            kx = hpatch[1, 1] - hpatch[0, 1]
            ky = (hpatch[1, 1] + hpatch[0, 1]) - 2*hc
        elif x_ + y_ > 1 and x_ - y_ > 0.:
            kx = hpatch[1, 1] - hpatch[1, 0]
            ky = (hpatch[1, 0] + hpatch[1, 1]) - 2*hc
        return hc + kx*(x_ - 0.5) + ky*(y_ - 0.5)
    return f
